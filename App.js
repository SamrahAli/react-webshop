
import './App.css';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavbarComp from './components/NavbarComp';
import CarouselComp from './components/CarouselComp';

import Product from './components/Product';
import {useState} from 'react'
import Banners from './components/banners'
import { Container } from 'react-bootstrap';
import { Image } from 'react-bootstrap'
function App() {
  
  return (
    <div className="App">
       <NavbarComp/>
     <CarouselComp />
     <Container fluid>
    <Product />  
    </Container> 
     {/* <Banners /> */}
        <Container>
            <figure className="position-relative">
            <a  href="#home"><Image src='banner1.png' alt='banner' className="img-fluid"/></a>
         
            <a  href="#home"><Image src='banner2.png' alt='banner' className="img-fluid" /></a>
            </figure>
          </Container> 
 
</div>
  );
}

export default App;
